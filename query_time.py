import ntplib
import datetime
import win32api
def setCurrentTime():
    client = ntplib.NTPClient()
    response = client.request('time.windows.com')
    currentTime = datetime.datetime.utcfromtimestamp(response.tx_time)
    year, month, day = currentTime.year, currentTime.month, currentTime.day
    hour, minute, second, millisecond = currentTime.hour, currentTime.minute, currentTime.second, int(str(currentTime.microsecond)[:3])
    dayOfWeek = currentTime.isoweekday()
    win32api.SetSystemTime(year, month , dayOfWeek , day , hour , minute , second , millisecond)

if __name__ == '__main__':
    setCurrentTime()