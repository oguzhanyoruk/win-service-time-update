import win32serviceutil
import win32evtlog
import win32event
import win32service
import time
import socket
import servicemanager
import sys
import threading
from query_time import setCurrentTime

class TimeSychronizerService(win32serviceutil.ServiceFramework):
    _svc_name_ = "TimeSychronizer"
    _svc_display_name_ = "Windows Time Sychronizer"
    _svc_description_ = "It sychronizes time for windwows because my windows can't do it"
    @classmethod
    def parseCommandLine(cls):
        win32serviceutil.HandleCommandLine(cls)

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        with open('D:/PersonalProjects/logs.txt', 'a') as file:
            file.write("Service has started" + '\n')
        socket.setdefaulttimeout(60)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        class SynchronizeEveryMinute(threading.Thread):
            def run(self):
                while True:
                    try:
                        setCurrentTime()
                    except:
                        with open('D:/PersonalProjects/logs.txt', 'a') as file:
                            file.write('Fuck windows NTP server!\n')
                    finally:
                        time.sleep(60)
        SynchronizeEveryMinute().run()


if __name__ == '__main__':
    TimeSychronizerService.parseCommandLine()